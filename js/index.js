"use strict";

const buttons = document.querySelectorAll(".btn");

document.addEventListener("keydown", keyPressingHandler);

function keyPressingHandler(event) {
  buttons.forEach((element) => {
    if (
      element.dataset.name === event.key ||
      element.dataset.name.toUpperCase() === event.key.toUpperCase()
    ) {
      if (element.classList.contains("btn-active")) {
        buttonsDisable();
        element.classList.remove("btn-active");
      } else {
        buttonsDisable();
        element.classList.add("btn-active");
      }
    }
  });
}

function buttonsDisable() {
  buttons.forEach((element) => {
    element.classList.remove("btn-active");
  });
}
